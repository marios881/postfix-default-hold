#!/bin/bash
postfix_queue=`mailq | grep Requests | awk {'print $5'}`
if [ -z "$postfix_queue" ]
then
	postfix_queue="0";
fi
if [ "$postfix_queue" -le "10" ]
then
        #all is quite
        #http://serverfault.com/questions/408482/postfix-hold-queue-release-x-oldest-messages-or-all-messages-queued-before-x-da
        mailq |egrep "^[[:alnum:]]{10}\!" |sort -k 4,4M -k 5,5n -k 6,6|head -4|awk '{print "mv /var/spool/postfix/hold/" substr($1, 1, length($1)-1) " /var/spool/postfix/incoming/" substr($1, 1, length($1)-1)}'|sh
        /usr/sbin/postfix flush
        echo 'OK'
        exit 0
elif [ "$postfix_queue" -ge "10" ] && [ "$postfix_queue" -le "20" ]
then
        #picking up some load better mail me with special header
        echo 'SEMI'
	mailq |egrep "^[[:alnum:]]{10}\!" |sort -k 4,4M -k 5,5n -k 6,6|head -4|awk '{print "mv /var/spool/postfix/hold/" substr($1, 1, length($1)-1) " /var/spool/postfix/incoming/" substr($1, 1, length($1)-1)}'|sh
        /usr/sbin/postfix flush
	/usr/sbin/sendmail -vt < /root/semi-mail.txt
        exit 1
else
        #Hell breaks loose
        #mail me with special header
        #wait 3 sec for mail delivery
        /usr/sbin/sendmail -vt < /root/high-mail.txt
        /usr/sbin/postfix flush
	sleep 10s
        /usr/sbin/service postfix stop
	#stop postfix
        echo 'FAIL'
        exit 2
fi