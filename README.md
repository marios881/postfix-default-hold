NOTHING WORKED, postfix does not want to throttle outgoing emails.

This will delay sending emails and put extra load on the cpu

<h2>What does this thing do</h2>
All outgoing email goes to HOLD queue except those that contain the secret mailfrom

<h2>Why, you ask?</h2>
My server normaly sends 1000-2000 emails per day, one day someone gets hacked and sends out 2million of them.<br>
If outgoing email workload is fairly predictable, when it spikes something is wrong

<h2>Instructions</h2>
This assumes you have a working postfix server and cron enabled, you know what you are doing and root access.<br><br>
Add in /etc/postfix/main.cf and copy header_checks to the same location<br>
<code>"header_checks = regexp:/etc/postfix/header_checks"</code>
<br>
<br>
make your own txt files or use sendmail directly<br>
the current postqueue.sh<br>
-will release XXX mails every 1 minute<br>
-alert when mailq is over XXX<br>
-alert and stop postfix when queue is over XXX<br>
<br>
edit the file to adjust to your needs<br>
<br>
postqueue.sh borrowed and adapted from https://lists.nongnu.org/archive/html/monit-general/2015-01/msg00037.html<br>
<br>
Now all outgoing email goes to HOLD queue except those that contain the secret mailfrom then gets moved on incoming by the following cron<br>
<br>
setup root cron<br>
<code>*       *       *       *       *       sh /root/postqueue.sh > /dev/null 2>&1</code>


Copyright (C) 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
